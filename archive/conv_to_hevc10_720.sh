#!/bin/bash

ARCH_DIR=$1

#ECHO="echo"
ECHO=""

TIMER='/usr/bin/time --format "Time taken = %E" -o /tmp/time'
X265PRESET="fast"
X265PARAMS="profile=main10"

if [ -z "$ARCH_DIR" ]; then
	echo "Please specify an archival directory"
	echo "Syntax: $(basename $0) <Archive Directory>"
	exit 1
fi

if [ ! -d "$ARCH_DIR" ]; then
	read -p "$ARCH_DIR doesn't exist. Create it? [y/n] " -n 1 -r
	if [[ ! $REPLY =~ ^[Yy]$ ]]
	then
	    exit 1
	else
		mkdir -p "$ARCH_DIR"
        if [ ! -d "$ARCH_DIR" ]; then
            #Something is very wrong with that directory!
            echo "Please check $ARCH_DIR, we tried creating it but it's not right."
            exit 1
        fi
	fi
fi

INDIRSIZE=$(du -sh)
DIDSOMETHING=false

for FILE in *.mkv ; do
	echo ""
    PROCESS=false
	RES_HT=$(avprobe "$FILE" 2>&1 | grep Stream.*Video | grep -oP ', \K[0-9]+x[0-9]+' | grep -oP [0-9]+$)
	HEVC=$(avprobe "$FILE" 2>&1 | grep Stream | grep -oPi hevc)
	if (( RES_HT > 900 )) ; then
		echo "$FILE is 1080p - $RES_HT"
		VIDEOBIT="-s hd720 -c:v libx265 -preset $X265PRESET -x265-params \"$X265PARAMS\" "
        PROCESS=true
	elif [ -z "$HEVC" ]; then
		VIDEOBIT="-c:v libx265 -preset $X265PRESET"
		echo "$FILE not 1080p, not HEVC - $RES_HT"
		PROCESS=true
	else
		VIDEOBIT="-c:v copy"
		echo "$FILE not 1080p, is HEVC - $RES_HT"
	fi

    AUDIO_TRACKS=$(avprobe "$FILE" 2>&1 | grep Stream.*Audio)
    NUM_AUDIOS=$(echo -e "$AUDIO_TRACKS" | wc -l)
    DTS_TRACK=$(echo -n "$AUDIO_TRACKS" | grep -oP DTS)

    if [ $NUM_AUDIOS -ne 1 ] ; then
    	echo "$FILE has more than one audio track - $NUM_AUDIOS audio tracks found!"
        echo "Skipping audio processing"
        AUDIOBIT="-c:a copy"
    elif [ ! -z "$DTS_TRACK" ]; then
        echo "$FILE has $NUM_AUDIOS audio track - DTS"
        PROCESS=true
        AUDIOBIT="-c:a ac3"
    else
        echo "$FILE has $NUM_AUDIOS audio track - not DTS"
        AUDIOBIT="-c:a copy"
    fi


    if $PROCESS ; then
    	INSIZE=$(ls -sh "$FILE" | cut -d " " -f 1)
		OUTFILE=$(echo "$FILE" | sed 's/1080/720/')
		
		$ECHO avconv -i "$FILE" -map 0 $VIDEOBIT $AUDIOBIT -c:s copy "/tmp/$OUTFILE"
		
		if [ $? -eq 0 ] ; then
			$ECHO mv -v "$FILE" "$ARCH_DIR/"
			$ECHO mv -v "/tmp/$OUTFILE" "$OUTFILE"

			OUTSIZE=$(ls -sh "$OUTFILE" | cut -d " " -f 1)
			echo "" # Let's show some stats!
			echo "in: $FILE = $INSIZE"
			echo "out: $OUTFILE = $OUTSIZE"
			#cat /tmp/time
			echo ""
			#rm /tmp/time
			DIDSOMETHING=true
		else
			echo "AVCONV EXITED WITH ERROR"
			$ECHO rm "/tmp/$OUTFILE"
		fi

	else
		echo "Nothing to do to $FILE"
    fi
    
done

if $DIDSOMETHING ; then
	OUTDIRSIZE=$(du -sh)
	echo ""
	echo "Starting directory size = $INDIRSIZE"
	echo "Finished directory size = $OUTDIRSIZE"
	echo ""
fi
