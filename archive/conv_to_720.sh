#!/bin/bash

ARCH_DIR=$1
SCR_NAME=$0

if [ -z "$ARCH_DIR" ]; then
	echo "Please specify an archival directory"
	echo "Syntax: $(basename $0) <Archive Directory>"
	exit 1
fi

if [ ! -d "$ARCH_DIR" ]; then
	read -p "$ARCH_DIR doesn't exist. Create it? [y/n] " -n 1 -r
	if [[ ! $REPLY =~ ^[Yy]$ ]]
	then
	    exit 1
	else
		mkdir -p "$ARCH_DIR"
	fi
fi

for FILE in *.mkv ; do
	echo ""
	RES_HT=$(avprobe "$FILE" 2>&1 | grep Stream | grep -oP ', \K[0-9]+x[0-9]+' | grep -oP [0-9]+$)
	if (( RES_HT > 900 )) ; then
		echo "$FILE is 1080p - $RES_HT"
		mv -v "$FILE" "$ARCH_DIR/"
		OUTFILE=$(echo "$FILE" | sed 's/1080/720/')
		avconv -i "$ARCH_DIR/$FILE" -s hd720 -c:v libx264 -preset medium -c:a copy -c:s copy "$OUTFILE"
	else
		echo "$FILE not 1080p - $RES_HT"
	fi
done