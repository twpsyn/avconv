#!/bin/bash

ARCH_DIR=$1
SCR_NAME=$0

if [ -z "$ARCH_DIR" ]; then
	echo "Please specify an archival directory"
	echo "Syntax: $(basename $0) <Archive Directory>"
	exit 1
fi

if [ ! -d "$ARCH_DIR" ]; then
	read -p "$ARCH_DIR doesn't exist. Create it? [y/n] " -n 1 -r
	if [[ ! $REPLY =~ ^[Yy]$ ]]
	then
	    exit 1
	else
		mkdir -p "$ARCH_DIR"
        if [ ! -d "$ARCH_DIR" ]; then
            #Something is very wrong with that directory!
            echo "Please check $ARCH_DIR, we tried creating it but it's not right."
            exit 1
        fi
	fi
fi

INDIRSIZE=$(du -sh)
DIDSOMETHING=false

for FILE in *.mkv ; do
	echo ""
    PROCESS=false
	RES_HT=$(avprobe "$FILE" 2>&1 | grep Stream | grep -oP ', \K[0-9]+x[0-9]+' | grep -oP [0-9]+$)
	if (( RES_HT > 900 )) ; then
		echo "$FILE is 1080p - $RES_HT"
		VIDEOBIT="-s hd720 -c:v libx264 -preset medium"
        PROCESS=true
	else
		VIDEOBIT="-c:v copy"
		echo "$FILE not 1080p - $RES_HT"
	fi

    AUDIO_TRACKS=$(avprobe "$FILE" 2>&1 | grep Stream.*Audio)
    NUM_AUDIOS=$(echo $AUDIO_TRACKS | wc -l)

    if [ $NUM_AUDIOS -ne 1 ] ; then
        echo "$FILE has more than one audio track - $NUM_AUDIOS audio tracks found!"
        echo "$AUDIO_TRACKS"
        exit 1
    fi

    DTS_TRACK=$(echo -n $AUDIO_TRACKS | grep -oP DTS)
    if [ ! -z "$DTS_TRACK" ]; then
        echo "$FILE has $NUM_AUDIOS audio track - DTS"
        PROCESS=true
        AUDIOBIT="-c:a ac3"
    else
        echo "$FILE has $NUM_AUDIOS audio track - not DTS"
        AUDIOBIT="-c:a copy"
    fi


    if $PROCESS ; then
    	INSIZE=$(ls -sh "$FILE" | cut -d " " -f 1)
		mv -v "$FILE" "$ARCH_DIR/"
		OUTFILE=$(echo "$FILE" | sed 's/1080/720/')
		avconv -i "$ARCH_DIR/$FILE" $VIDEOBIT $AUDIOBIT -c:s copy "$OUTFILE"
		OUTSIZE=$(ls -sh "$OUTFILE" | cut -d " " -f 1)
		echo "" # Let's show some stats!
		echo "$FILE = $INSIZE"
		echo "$OUTFILE = $OUTSIZE"
		echo ""
		DIDSOMETHING=true
	else
		echo "Nothing to do to $FILE"
    fi
    
done

if $DIDSOMETHING ; then
	OUTDIRSIZE=$(du -sh)
	echo ""
	echo "Starting directory size = $INDIRSIZE"
	echo "Finished directory size = $OUTDIRSIZE"
	echo ""
fi
