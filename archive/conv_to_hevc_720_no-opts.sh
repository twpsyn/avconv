#!/bin/bash

# converts non-HEVC encoded videos to HEVC
# converts 1080p to 720p
# converts DTS to AC3 if only one audio track and it's DTS
# converts AVI to MKV (HEVC)

ARCH_DIR=$1

X265PARAM="-x265-params log-level=1"
X265PRESET="-preset fast"

INDIRSIZE=$(du -sh)
DIDSOMETHING=false

#ECHO="echo"
ECHO=""


# Before we do anything else, check if there are processable video files here
# Processable = MKV, MP4, M4V, or AVI

NOPROCESS=true
PROCAVI=false

for f in *.mp4; do
    [ -e "$f" ] && NOPROCESS=false
    break
done

for f in *.mkv; do
    [ -e "$f" ] && NOPROCESS=false
    break
done

for f in *.m4v; do
    [ -e "$f" ] && NOPROCESS=false
    break
done

for f in *.avi; do
	[ -e "$f" ] && PROCAVI=true
	break
done

if [ "$NOPROCESS" = "true" ] && [ "$PROCAVI" = "false" ] ; then
    echo "No processable video files in folder \"$(pwd)\""
    exit 1
fi

# We have something to do, let's proceed

# Check archive directory
if [ -z "$ARCH_DIR" ]; then
	echo "Please specify an archival directory"
	echo "Syntax: $(basename $0) <Archive Directory>"
	exit 1
fi

if [ ! -d "$ARCH_DIR" ]; then
	read -p "$ARCH_DIR doesn't exist. Create it? [y/n] " -n 1 -r
	if [[ ! $REPLY =~ ^[Yy]$ ]]
	then
	    exit 1
	else
		mkdir -p "$ARCH_DIR"
        if [ ! -d "$ARCH_DIR" ]; then
            #Something is very wrong with that directory!
            echo "Please check $ARCH_DIR, we tried creating it but it's not right."
            exit 1
        fi
	fi
fi


if $PROCAVI ; then
	echo "Processing AVI file(s)"
	echo ""
	for FILE in *.avi ; do
		INSIZE=$(ls -sh "$FILE" | cut -d " " -f 1)
		OUTFILE=$(echo "$FILE" | sed 's/avi$/mkv/')
		echo "Converting \"$FILE\" to MKV"
		echo ""
		$ECHO avconv -hide_banner -i "$FILE" -map 0 -c:v libx265 $X265PRESET $X265PARAM -c:a copy "/tmp/$OUTFILE"

		if [ $? -eq 0 ] ; then
			$ECHO mv -v "$FILE" "$ARCH_DIR/"
			$ECHO mv -v "/tmp/$OUTFILE" "$OUTFILE"

			OUTSIZE=$(ls -sh "$OUTFILE" | cut -d " " -f 1)
			echo "" # Let's show some stats!
			echo "in: $FILE = $INSIZE"
			echo "out: $OUTFILE = $OUTSIZE"
			#cat /tmp/time
			echo ""
			#rm /tmp/time
			DIDSOMETHING=true
			#NOPROCESS=false
		else
			echo "AVCONV EXITED WITH ERROR"
			$ECHO rm "/tmp/$OUTFILE"
		fi

	done
fi

if [ "$NOPROCESS" = "false" ] ; then
	for FILE in *.m[kp4][v4] ; do
		echo ""
	    PROCESS=false
		RES_HT=$(avprobe -hide_banner -select_streams v:0 "$FILE" 2>&1 | grep Stream.*Video | grep -oP ', \K[0-9]+x[0-9]+' | grep -oP [0-9]+$)
		HEVC=$(avprobe "$FILE" 2>&1 | grep Stream | grep -oPi hevc)
		if (( RES_HT > 900 )) ; then
			echo "$FILE is 1080p - $RES_HT"
			VIDEOBIT="-s hd720 -c:v copy -c:v:0 libx265 $X265PRESET $X265PARAM"
	        PROCESS=true
		elif [ -z "$HEVC" ]; then
			VIDEOBIT="-c:v copy -c:v:0 libx265 $X265PRESET $X265PARAM"
			echo "$FILE not 1080p, not HEVC - $RES_HT"
			PROCESS=true
		else
			VIDEOBIT="-c:v copy"
			echo "$FILE not 1080p, is HEVC - $RES_HT"
		fi

	    AUDIO_TRACKS=$(avprobe "$FILE" 2>&1 | grep Stream.*Audio)
	    NUM_AUDIOS=$(echo -e "$AUDIO_TRACKS" | wc -l)
	    DTS_TRACK=$(echo -n "$AUDIO_TRACKS" | grep -oP DTS)

	    if [ $NUM_AUDIOS -ne 1 ] ; then
	    	echo "$FILE has more than one audio track - $NUM_AUDIOS audio tracks found!"
	        echo "Skipping audio processing"
	        AUDIOBIT="-c:a copy"
	    elif [ ! -z "$DTS_TRACK" ]; then
	        echo "$FILE has $NUM_AUDIOS audio track - DTS"
	        PROCESS=true
	        AUDIOBIT="-c:a ac3"
	    else
	        echo "$FILE has $NUM_AUDIOS audio track - not DTS"
	        AUDIOBIT="-c:a copy"
	    fi


	    if $PROCESS ; then
	    	INSIZE=$(ls -sh "$FILE" | cut -d " " -f 1)
			OUTFILE=$(echo "$FILE" | sed 's/1080/720/')
			
			$ECHO avconv -hide_banner -i "$FILE" -map 0 $VIDEOBIT $AUDIOBIT -c:s copy "/tmp/$OUTFILE"
			
			if [ $? -eq 0 ] ; then
				$ECHO mv -v "$FILE" "$ARCH_DIR/"
				$ECHO mv -v "/tmp/$OUTFILE" "$OUTFILE"

				OUTSIZE=$(ls -sh "$OUTFILE" | cut -d " " -f 1)
				echo "" # Let's show some stats!
				echo "in: $FILE = $INSIZE"
				echo "out: $OUTFILE = $OUTSIZE"
				#cat /tmp/time
				echo ""
				#rm /tmp/time
				DIDSOMETHING=true
			else
				echo "AVCONV EXITED WITH ERROR"
				$ECHO rm "/tmp/$OUTFILE"
			fi

		else
			echo "Nothing to do to $FILE"
	    fi
	    
	done
fi

if $DIDSOMETHING ; then
	OUTDIRSIZE=$(du -sh)
	echo ""
	echo "Starting directory size = $INDIRSIZE"
	echo "Finished directory size = $OUTDIRSIZE"
	echo ""
fi
