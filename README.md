# AVConv #

This is my script to handle converting video files into H.265 (HEVC).

## conv_with_opts.sh

__Usage:__

_Arguments expected where indicated with <>, defaults in []_

YOU MUST SPECIFY ONE (AND ONLY ONE) OF THE FOLLOWING TWO

```
-d - process current directory
-f <filename> - process single file
```

THE REST ARE OPTIONAL
```
-j - just do it (no prompting) [disabled]
-n - no action (just echo commands) [disabled]
-s - print statistics (file and dir sizes pre and post processing) [disabled]
-v - verbose [disabled]
-7 - downscale 1080 to 720 [disabled]
-a - process audio where possible (DTS -> AC3, if only one audio stream) [disabled]
-b <directory> - backup directory [none]
-t <directory> - temporary directory [/tmp]
-l <0-4> - x265 log level [1 - warnings and errors]
    * see http://x265.readthedocs.io/en/default/cli.html#logging-statistic-options
-p <preset> - x265 preset [fast]
    * see https://wiki.libav.org/Encoding/hevc
```

For example, ```conv_with_opts.sh -dj7av -b /archives``` will have the script:

+ process all files in the current directory
+ "just do it" ie, no prompting
+ convert to 720p where appropriate
+ convert the audio where appropriate and possible
+ be verbose
+ copy a backup to the /archives directory


## colours.sh
This is a utility script that allows for colours in the output of the above script.

I'm not crazy for colours in my terminal, but there are coloured asterisks at the start of lines to indicate info/error etc.
