#!/usr/bin/env bash

# General purpose video converstion script
# Will convert video to HEVC, plus specified options

# bring in colour definitions
source ~/scripts/my\ scripts/colours.sh


ECHO=""
PROCFILE=false
THISFILE=""
PROCDIR=false
VERBOSE=false
PRSTATS=false
PROMPT=true
BACKUP=false
ARCH_DIR="/dev/null"
TMPDIR="/tmp"
OUTFILE=""
DOWNSCALE=false
PROCAUDIO=false
LOGLEVEL=1
PRESET="fast"

# state tracking
DIDSOMETHING=false

## FUNCTION DEFINITIONS ##

function print_help() {
	echo ""
	echo " Usage:"
	echo ""
	echo " Arguments expected where indicated with <>, defaults in []"
	echo ""
	echo " ** YOU MUST SPECIFY ONE (AND ONLY ONE) OF THE FOLLOWING TWO"
	echo ""
	echo " -d - process current directory"
	echo " -f <filename> - process single file"
	echo ""
	echo " ** THE REST ARE OPTIONAL"
	echo ""
	echo " -j - just do it (no prompting) [disabled]"
	echo " -n - no action (just echo commands) [disabled]"
	echo " -s - print statistics (file and dir sizes pre and post processing) [disabled]"
	echo " -v - verbose [disabled]"
	echo " -7 - downscale 1080 to 720 [disabled]"
	echo " -a - process audio where possible (DTS -> AC3, if only one audio stream) [disabled]"
	echo " -b <directory> - backup directory [none]"
	echo " -t <directory> - temporary directory [/tmp]"
	echo " -l <0-4> - x265 log level [1 - warnings and errors]"
	echo "    * see http://x265.readthedocs.io/en/default/cli.html#logging-statistic-options"
	echo " -p <preset> - x265 preset [fast]"
	echo "    * see https://wiki.libav.org/Encoding/hevc"
	echo ""
	echo ""
}


#####
# Gives us our output filename.
# Replaces .avi with .mkv
# If downscaling also replaces 1080 with 720
#
# GLOBALS:
#	OUTFILE
#	DOWNSCALE (doesn't modify)
# ARGUMENTS:
#	Input file name
#####
function get_out_filename() {
	INFILE=$1
	if $DOWNSCALE ; then
		OUTFILE=$(echo "$FILE" | sed 's/.[[:alnum:]]*$/.mkv/' | sed 's/1080/720/')
	else
		OUTFILE=$(echo "$FILE" | sed 's/.[[:alnum:]]*$/.mkv/')
	fi
}


######
# Checks for existence of specified backup (archive) directory
# If it doesn't exist then it'll try to create it and check again
# If there is still a problem then it'll cause the script to exit
#
# GLOBALS:
#	ARCH_DIR (doesn't modify)
#	PROMPT (doesn't modify)
# ARGUMENTS:
#	None
#####
function check_arch_dir() {

	if [ ! -d "$ARCH_DIR" ]; then
		if $PROMPT ; then
			read -p "$ARCH_DIR doesn't exist. Create it? [y/n] " -n 1 -r
		else
			REPLY="Y"
		fi

		if [[ ! $REPLY =~ ^[Yy]$ ]]
		then
		    exit 1
		else
			mkdir -p "$ARCH_DIR"
	        if [ ! -d "$ARCH_DIR" ]; then
	            #Something is very wrong with that directory!
	            echo -e "${RED}ERROR${NC}: Please check $ARCH_DIR."
	            echo "       We tried creating it but it's not right."
	            exit 1
	        fi
		fi
		echo ""
	fi

}


#####
# Converts a file according to specified options
#
# GLOBALS:
#	DOWNSCALE
#	PROCAUDIO
#	VERBOSE
#	PRSTATS
#	BACKUP
#	ARCH_DIR
#	OUTFILE
#	TMPDIR
#	ECHO
#	DIDSOMETHING
#	X265PRESET
#	X265PARAM
# ARGUMENTS:
#	The file to be processed
#####
function proc_file() {
	FILE=$1
	PROCESS=false

	#VIDSTREAM=$(avprobe -hide_banner -select_streams v:0 "$FILE" 2>&1 | grep Stream.*Video)
	#RES_HT=$( echo $VIDSTREAM | grep -oP ', \K[0-9]+x[0-9]+' | grep -oP [0-9]+$)
	#HEVC=$(echo $VIDSTREAM | grep -oPi hevc)

	# if DOWNSCALE and res > 900
	#	it's 1080p and we're downscaling
	#	action = convert to hevc @ 720p
	# elif not hevc
	#	either it's not 1080p or we're not downscaling
	#	it's not hevc
	#	action = convert to hevc @ current resolution
	# else
	#	either it's not 1080p or we're not downscaling
	#	it's already hevc
	#	action = copy it

	VIDEOBIT="-c:v copy"
	AUDIOBIT="-c:a ac3"

	PROCESS=true

    if $PROCESS ; then

    	$PRSTATS && INSIZE=$(ls -sh "$FILE" | cut -d " " -f 1)
    	$BACKUP && check_arch_dir $ARCH_DIR

    	get_out_filename $FILE
		TMPFILE="$TMPDIR/$OUTFILE"

		echo ""

		$ECHO avconv -hide_banner -i "$FILE" -map 0 $VIDEOBIT $AUDIOBIT -c:s copy "$TMPFILE"

		if [ $? -eq 0 ] ; then

			if $VERBOSE ; then
				MVCMD="mv -v"
				RMCMD="rm -v"
			else
				MVCMD="mv"
				RMCMD="rm"
			fi

			if $BACKUP ; then
				$ECHO $MVCMD "$FILE" "$ARCH_DIR/"
			else
				$ECHO $RMCMD "$FILE"
			fi
			$ECHO $MVCMD "$TMPFILE" "$OUTFILE"

			if $PRSTATS ; then
				OUTSIZE=$(ls -sh "$OUTFILE" | cut -d " " -f 1)
				echo "" # Let's show some stats!
				echo "in: $FILE = $INSIZE"
				echo "out: $OUTFILE = $OUTSIZE"
				#cat /tmp/time
				echo ""
			fi

			DIDSOMETHING=true
		else
			echo -e "${RED}ERROR${NC}: AVCONV EXITED WITH ERROR"
			$ECHO rm "/tmp/$OUTFILE"
		fi

	else
		$VERBOSE && echo -e "${GREEN}*${NC} Nothing to do to $FILE"
    fi

}


#####
# Processes all files in current directory
# Calls proc_file on every processable video file
#
# GLOBALS:
#	VERBOSE
#	PRSTATS
#	DIDSOMETHING
# ARGUMENTS:
#	None
#####
function proc_dir() {

	NOPROCESS=true
	PROCAVI=false

	for f in *.mp4; do
	    [ -e "$f" ] && NOPROCESS=false
	    break
	done

	for f in *.mkv; do
	    [ -e "$f" ] && NOPROCESS=false
	    break
	done

	for f in *.m4v; do
	    [ -e "$f" ] && NOPROCESS=false
	    break
	done

	for f in *.avi; do
		[ -e "$f" ] && PROCAVI=true
		break
	done

	if [ "$NOPROCESS" = "true" ] && [ "$PROCAVI" = "false" ] ; then
	    $VERBOSE && echo "No processable video files in folder \"$(pwd)\""
	    exit 1
	fi

	$PRSTATS && INDIRSIZE=$(du -sh)

	if $PROCAVI ; then
		$VERBOSE && echo "Processing AVI file(s)"
		for FILE in *.avi ; do
			proc_file "$FILE"
		done
	fi

	if [ "$NOPROCESS" = "false" ] ; then
		for FILE in *.m[kp4][v4] ; do
			proc_file "$FILE"
		done
	fi

	if $DIDSOMETHING && $PRSTATS ; then
		OUTDIRSIZE=$(du -sh)
		echo ""
		echo "Starting directory size = $INDIRSIZE"
		echo "Finished directory size = $OUTDIRSIZE"
		echo ""
	fi

}


## END OF FUNCTION DECLARATIONS ##

if [[ -z $1 ]] ; then
	echo -e "${RED}ERROR${NC}: Must specify -d or -f, -h for help"
	print_help
	exit 1
fi

while getopts 'hdjnsv7af:b:t:l:p:' flag; do
	case "${flag}" in
		h)
			print_help
			exit
			;;
		f)
			if $PROCDIR ; then
				echo -e "${RED}ERROR${NC}: Illegal option combination - \"f\" and \"d\""
				print_help
				exit 1
			fi
			THISFILE="${OPTARG}"
			PROCFILE=true
			;;
		d)
			if $PROCFILE ; then
				echo -e "${RED}ERROR${NC}: Illegal option combination - \"f\" and \"d\""
				print_help
				exit 1
			fi
			PROCDIR=true
			;;
		j)
			PROMPT=false
			;;
		n)
			ECHO="echo"
			;;
		b)
			BACKUP=true
			ARCH_DIR="${OPTARG}"
			;;
		t)
			TMPDIR="${OPTARG}"
			;;
		s) PRSTATS=true ;;
		v) VERBOSE=true ;;
		l)
			LOGLEVEL="${OPTARG}"
			if ! [[ $LOGLEVEL =~ ^[0-4]$ ]] ; then
				echo -e "${RED}ERROR${NC}: invalid log level \"$LOGLEVEL\""
				print_help
				exit 1
			fi
			;;
		p)
			case "${OPTARG}" in
				"ultrafast") PRESET="${OPTARG}" ;;
				"superfast") PRESET="${OPTARG}" ;;
				"veryfast") PRESET="${OPTARG}" ;;
				"faster") PRESET="${OPTARG}" ;;
				"fast") PRESET="${OPTARG}" ;;
				"medium") PRESET="${OPTARG}" ;;
				"slow") PRESET="${OPTARG}" ;;
				"slower") PRESET="${OPTARG}" ;;
				"veryslow") PRESET="${OPTARG}" ;;
				"placebo") PRESET="${OPTARG}" ;;
				*)
					echo -e "${RED}ERROR${NC}: ${OPTARG} is not a valid preset"
					print_help
					exit 1
					;;
			esac
			;;
		7) DOWNSCALE=true ;;
		a) PROCAUDIO=true ;;
		#:)
		#	echo "ERROR: $OPTARG requires an argument"
		#	exit 1
		#	;;
		\?)
			echo -e "${RED}ERROR${NC}: Invalid options"
			print_help
			exit 1
			;;
	esac
done

if $VERBOSE ; then
	echo ""
	$PROCFILE && echo -e " ${BROWN}*${NC} Processing \"$THISFILE\""
	$PROCDIR && echo -e " ${BROWN}*${NC} Processing all files in current directory"
	echo ""
	echo -e " ${GREEN}*${NC} Verbose mode on"
	if $PROMPT ; then
		echo -e " ${GREEN}*${NC} Prompting enabled"
	else
		echo -e " ${RED}*${NC} Prompting disabled"
	fi
	if [[ $ECHO == "echo" ]] ; then
		echo -e " ${GREEN}*${NC} Trial run (print commands only)"
	else
		echo -e " ${RED}*${NC} Actually making changes"
	fi
	if $BACKUP ; then
		echo -e " ${GREEN}*${NC} Saving backups to \"$ARCH_DIR\""
	else
		echo -e " ${RED}*${NC} Not saving backups"
	fi
	echo -e " ${BROWN}*${NC} Temporary directory = \"$TMPDIR\""
	if $PRSTATS ; then
		echo -e " ${GREEN}*${NC} Printing file and directory statistics"
	else
		echo -e " ${RED}*${NC} Not printing statistics"
	fi
	echo -e " ${BROWN}*${NC} x265 log level = \"$LOGLEVEL\""
	echo -e " ${BROWN}*${NC} x265 preset = \"$PRESET\""
	if $DOWNSCALE ; then
		echo -e " ${GREEN}*${NC} Converting 1080p to 720p"
	else
		echo -e " ${RED}*${NC} Leaving 1080p at 1080p"
	fi
	if $PROCAUDIO ; then
		echo -e " ${GREEN}*${NC} Processing audio"
	else
		echo -e " ${RED}*${NC} Not processing audio"
	fi

	echo ""
	if $PROMPT ; then
			read -p "Proceed? [y/n] " -n 1 -r
			echo ""
		if [[ ! $REPLY =~ ^[Yy]$ ]]
		then
			exit 1
		fi
	fi
	echo ""

fi


X265PARAM="-x265-params log-level=$LOGLEVEL"
X265PRESET="-preset $PRESET"


if $PROCDIR ; then
	proc_dir
elif $PROCFILE ; then
	proc_file "$THISFILE"
else
	echo -e "${RED}ERROR${NC}: Must specify -d or -f"
	print_help
fi
